﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Bacchus.Models;

namespace Bacchus.Controllers
{

    public class HomeController : Controller

    {
        private BacchusData db = new BacchusData();

        public ActionResult Index()
        {
            AuctionObject[] items = GetItems();
            return View(items.ToList());

            
        }

        public ActionResult SortedIndex(string id)
        {

            AuctionObjectModel model = new AuctionObjectModel();
            if (id == null)
            {
                AuctionObject[] items = GetItems();
                model.AuctionObjects = items.ToList();
            }
            else
            {
                AuctionObject[] items = GetItems();
                var categoryItems = items.Where(x => x.productCategory == id);
                model.AuctionObjects = categoryItems.ToList();
                model.ProductCategory = id;
            }
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        private const string URL = "http://uptime-auction-api.azurewebsites.net/api/Auction";
        private string urlParameters = "";

        public AuctionObject[] GetItems()
        {

            System.Diagnostics.Debug.WriteLine("algus");
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
            if (response.IsSuccessStatusCode)
            {

                string responseBody = response.Content.ReadAsStringAsync().Result;
                System.Diagnostics.Debug.WriteLine(responseBody);


                AuctionObject[] objects = JsonConvert.DeserializeObject<AuctionObject[]>(responseBody);
                System.Diagnostics.Debug.WriteLine("serialize");
                System.Diagnostics.Debug.WriteLine(objects.Length);
                System.Diagnostics.Debug.WriteLine(objects[0].biddingEndDate);
                return objects;

            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }

            //Make any other calls using HttpClient here.

            //Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
            client.Dispose();
            return null;
        }


    }
}