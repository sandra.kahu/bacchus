﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Bacchus.Models;
using static Bacchus.Models.Bid;


namespace Bacchus.Controllers
{
    public class BidsController : Controller
    {
        private BacchusData db = new BacchusData();

        // GET: Bids
        [Authorize]
        public ActionResult Index()
        {
            var user = db.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault();
            if (User.IsInRole("Admin"))
            {
                return View(db.Bids.ToList());
            }
            else
            {
                return View(db.Bids.ToList().Where(x => x.UserId == user.Id));
            }

        }

        [Authorize]
        // GET: Bids/Create
        public ActionResult Create(string ProductID, string UserId)
        {
            var user = db.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault();
            
            return View(new Bid
            {
                BidDate = DateTime.Now,
                UserId = user.Id
            });
        }
        [Authorize]
        // POST: Bids/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserId,BidAmount,BidDate,ProductId")] Bid bid)
        {
            if (ModelState.IsValid)
            {
                db.Bids.Add(bid);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

           

            return View(bid);
        }

        


    }
}
