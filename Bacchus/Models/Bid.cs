﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;


namespace Bacchus.Models
{
    public class Bid
    {
        public int Id { get; set; }
        [Display(Name = "UserId")]
        public string UserId { get; set; }
        
        private DateTime _bidDate = DateTime.Now;
        [Display(Name = "Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public DateTime BidDate
        {
            get
            {
                return (_bidDate == DateTime.MinValue) ? DateTime.Now : _bidDate;
            }

            set { _bidDate = value; }
        }

        [Display(Name = "Amount")]
        public decimal BidAmount { get; set; }
        public string ProductId { get; set; }
        
        public virtual ApplicationUser User { get; set; }

        
    }
}