﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Bacchus.Models
{

    public class AuctionObjectModel
    {
        [Display(Name = "Category")]
        public string ProductCategory { get; set; }
        public List<AuctionObject> AuctionObjects { get; set; }

    }
    public class AuctionObject
    {
        [Display(Name = "Product Id")]
        public string productId { get; set; }
        [Display(Name = "Item")]
        public string productName { get; set; }
        [Display(Name = "Description")]
        public string productDescription { get; set; }
        [Display(Name = "Category")]
        public string productCategory { get; set; }
        [Display(Name = "Bidding end date")]
        public DateTime biddingEndDate { get; set; }

        // Ülemised muutujad väikse algustähega, sest et ASP Clienti-s muutuja nimed väikeste tähtedega.

        public DateTime BiddingEndDateLocal => biddingEndDate.ToLocalTime();

    }


}