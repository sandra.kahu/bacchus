﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;

namespace Bacchus.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
        [Display(Name = "First name")]
        public string FirstName { get; set; }
        [Display(Name = "Last name")]
        public string LastName { get; set; }
        public string FullName { get; set; }
    }

    public class BacchusData : IdentityDbContext<ApplicationUser>
    {
        public BacchusData()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static BacchusData Create()
        {
            return new BacchusData();
        }

        public System.Data.Entity.DbSet<Bacchus.Models.AspNetUser> AspNetUsers { get; set; }
        public System.Data.Entity.DbSet<Bacchus.Models.Bid> Bids { get; set; }
        public  DbSet<AspNetRole> AspNetRoles { get; set; }
    }
}
