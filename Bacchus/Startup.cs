﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Bacchus.Startup))]
namespace Bacchus
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
