﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Bacchus.Models;

namespace Bacchus.App_Start
{
    
    public static class SharedLogic
    {
        private const string URL = "http://uptime-auction-api.azurewebsites.net/api/Auction";
        private static string urlParameters = "";
        public static IEnumerable<IGrouping<string, AuctionObject>> GetMenuObjects()
        {
            System.Diagnostics.Debug.WriteLine("algus");
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
            if (response.IsSuccessStatusCode)
            {

                string responseBody = response.Content.ReadAsStringAsync().Result;
                System.Diagnostics.Debug.WriteLine(responseBody);


                AuctionObject[] objects = JsonConvert.DeserializeObject<AuctionObject[]>(responseBody);
                System.Diagnostics.Debug.WriteLine("serialize");
                System.Diagnostics.Debug.WriteLine(objects.Length);
                System.Diagnostics.Debug.WriteLine(objects[0].biddingEndDate);
                return objects.ToList().GroupBy(x => x.productCategory).OrderBy(x => x.Key);

            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }

            //Make any other calls using HttpClient here.

            //Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
            client.Dispose();
            return null;

        }


    }
}